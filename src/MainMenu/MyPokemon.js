import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import "./PokemonList.css";

class MyPokemon extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pokemonlist: []
        }
    }

    componentDidMount() {
        if (localStorage.getItem("mypokemon")) {
            console.log("myp",localStorage.getItem("mypokemon"))
            this.setState({
                pokemonlist: JSON.parse(localStorage.getItem("mypokemon"))
            })
        }
    }

    removePokemon = async (data, i) => {
        console.log(data)
        console.log(i)
        let name = data.name;
        let nickname = data.nickname;
        let array = this.state.pokemonlist
        array.splice(i,1)
        await this.setState({pokemonlist: array})
        await localStorage.setItem("mypokemon",JSON.stringify(this.state.pokemonlist))
        alert(name + " (" + nickname + ") has been released to the wild")
    }

    render() {
        const { pokemonlist } = this.state
        return (
            <React.Fragment>
                <div className="image-grid">
                {pokemonlist ? pokemonlist.map((data, i) => (
                    <div className="image-item ml-0">
                        <Card>
                            <Card.Body>
                                <Card.Title>{String(data.name).toUpperCase()}</Card.Title>
                                <Card.Text>
                                    <p>({data.nickname})</p>
                                    <div style={{ textAlign: 'center' }}>
                                        <img src={data.image} style={{ height: '100px', width: '100px' }} />
                                    </div>
                                </Card.Text>
                                <Button variant="primary" onClick={() => this.removePokemon(data, i)}>Release To The Wild</Button>
                            </Card.Body>
                        </Card>
                    </div>
                )) : <></>}
                </div>
                {pokemonlist.length == 0 ? <p style={{textAlign:'center'}}>No pokemons here, lets capture some</p> : <></>}
            </React.Fragment>
        )
    }
}
export default MyPokemon