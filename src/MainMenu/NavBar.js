import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import PokemonList from "./PokemonList";
import MyPokemon from "./MyPokemon";
import Logo from "../logo.svg";

class NavBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            menu:"pokemonlist"
        }
    }

    componentDidMount() {
    }

    // componentWillReceiveProps(test) {
    //     this.setState({
    //         ...this.state,
    //         isopen: test.onModal
    //     })
    // }
    

    render() {
        const { fields } = this.state
        return (
            <div>
                
            <Navbar sticky="top" collapseOnSelect expand="lg" bg="dark" variant="dark">
            <img
                src={Logo}
                width="30"
                height="30"
                className="d-inline-block align-top"
                alt="React Bootstrap logo"
            />
            <Navbar.Brand>PokeApp Nanda</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link onClick={()=>this.setState({menu:"pokemonlist"})}>Pokemon List</Nav.Link>
                    <Nav.Link onClick={()=>this.setState({menu:"mypokemon"})}>My Pokemon</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
        {
            (this.state.menu == 'pokemonlist' ? 
            <PokemonList/> : 
            <MyPokemon/>)
        }
        </div>
        )
    }
}
export default NavBar