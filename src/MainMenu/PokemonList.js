import React, { Component } from "react";
import serviceapi from "../controller/serviceapi";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import "./PokemonList.css";

class MainMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nexturl : "",
      showdetail: false,
      showdialog: false,
      pokemonlist: [],
      pokemondetail: {
        abilities: [],
        image: '',
        name: '',
        types: [],
        nickname: ''
      },
    };
  }

  // trackscroll(){
  //     if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
  //        console.log("bott");
  //     }
  //  }

  componentDidMount() {
    this.getData();
    // window.addEventListener('scroll', this.trackscroll());
  }

  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.trackscroll())
  // }

  handleCloseDetail = () => {
    this.setState({
      showdetail: false
    })
  }

  handleCloseDialog = () => {
    this.setState({
      showdialog: false,
      showdetail: true
    })
  }

  getData = async () => {
    let dataRes = await serviceapi.get("/pokemon?offset=0&limit=20");
    this.setState({ 
      pokemonlist: dataRes.results,
      nexturl: dataRes.next.substring(25, dataRes.next.length) });
  };

  loadMore = async () => {
    let dataRes = await serviceapi.get(this.state.nexturl);
    if(dataRes.results){
      let array = this.state.pokemonlist;
      dataRes.results.map(data => {
        array.push(data);
      })
      this.setState({
        pokemonlist: array,
        nexturl: dataRes.next.substring(25, dataRes.next.length)})
      }
    }

  getDetail = async (url) => {
    let substr = url.substring(25, url.length - 1)
    let dataRes = await serviceapi.get(substr)
    this.setState({
      pokemondetail: {
        ...this.state.pokemondetail,
        abilities: dataRes.abilities,
        name: dataRes.species.name,
        image: dataRes.sprites.front_default,
        types: dataRes.types
      },
      showdetail: true
    })
  }

  handleChangeInput = (e) => {
    this.setState({
      pokemondetail:{
        ...this.state.pokemondetail,
        nickname: e.target.value
      }
    })
  }

  capturePokemon = () => {
    if (Math.random() < 0.5) {
      alert("Capture Failed")
    } else {
      // alert("Show capture modal")
      this.setState({showdialog: true, showdetail: false})
    }
  }

  handleSubmitNickName = async (e) => {
    e.preventDefault();
    let datalocal = localStorage.getItem("mypokemon");
    if(datalocal) {
      datalocal = JSON.parse(datalocal)
      let flag = 0
      datalocal.map(data => {
        if(data.nickname == this.state.pokemondetail.nickname){
          flag = 1
        }
      })
      if(flag == 1) {
        alert("NIckname already exist");
      } else {
        let temp = [...datalocal,this.state.pokemondetail]
          datalocal = temp
          localStorage.setItem("mypokemon",JSON.stringify(datalocal))
          this.setState({showdialog: false})
          alert("Pokemon captured")
      }
    } else {
      datalocal = [this.state.pokemondetail] 
      await localStorage.setItem("mypokemon",JSON.stringify(datalocal))
      this.setState({showdialog: false})
      alert("Pokemon captured")
    }
  }

  isNumeric(e) {
    var keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
    var ret = keyCode >= 48 && keyCode <= 57;
    if (!ret) {
      e.preventDefault();
    }
  }

  isAlphaNumeric(e) {
    var keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
    var ret =
      (keyCode >= 48 && keyCode <= 57) ||
      (keyCode >= 65 && keyCode <= 90) ||
      (keyCode >= 97 && keyCode <= 122) ||
      keyCode === 32;
    if (!ret) {
      e.preventDefault();
    }
  }

  isAlphabet(e) {
    var keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
    var ret =
      keyCode > 32 &&
      (keyCode < 65 || keyCode > 90) &&
      (keyCode < 97 || keyCode > 122);
    if (ret) {
      e.preventDefault();
    }
  }

  // componentWillReceiveProps(test) {
  //     this.setState({
  //         ...this.state,
  //         isopen: test.onModal
  //     })
  // }

  render() {
    const { pokemondetail, pokemonlist, showdetail, showdialog } = this.state;
    return (
      <React.Fragment>
        <div className="image-grid ml-4 mr-4">
        {pokemonlist.map((data) => (
          <div className="image-item">
            <Card>
              {/* <Card.Header as="h5">{data.name}</Card.Header> */}
              <Card.Body>
                <Card.Title>{String(data.name).toUpperCase()}</Card.Title>
                <Button variant="primary" onClick={() => this.getDetail(data.url)}>Details</Button>
              </Card.Body>
            </Card>
          </div>
        ))}
        </div>
        <Button className="col-md-12" onClick={()=>this.loadMore()}>Load More</Button>

        <Modal show={showdetail} onHide={() => this.handleCloseDetail()}>
          <Modal.Header closeButton>
            <Modal.Title>{String(pokemondetail.name).toUpperCase()}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div style={{ textAlign: 'center' }}>
              <img src={pokemondetail.image} style={{ height: '200px', width: '200px' }} />
            </div>
            <p>
              Skills :
              </p>
            <ul>
              {pokemondetail.abilities.map(data => (
                <li>{String(data.ability.name).replace("-"," ").toUpperCase()}</li>
              )
              )}
            </ul>
            <p>
              Types :
              </p>
            <ul>
              {pokemondetail.types.map(data => (
                <li>{String(data.type.name).replace("-"," ").toUpperCase()}</li>
              )
              )}
            </ul>
            <Button onClick={() => this.capturePokemon()} className="col-md-12">Capture!</Button>
          </Modal.Body>
        </Modal>

        <Modal show={showdialog} onHide={() => this.handleCloseDialog()}>
          <Modal.Header closeButton>
            <Modal.Title>{String(pokemondetail.name).toUpperCase()}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={this.handleSubmitNickName}>
              <Form.Group controlId="nickname">
                <Form.Label>Nickname</Form.Label>
                <Form.Control 
                  type="text" 
                  required 
                  name="nickname"
                  placeholder="Pokemon Nickname"
                  // value={this.state.pokemondetail.nickname}
                  onChange={(e)=>this.handleChangeInput(e)}
                  />
                <Form.Text className="text-muted">
                  You cannot enter the same nickname twice.
                </Form.Text>
              </Form.Group>
              <Button type="submit">Confirm</Button>
            </Form>
          </Modal.Body>
        </Modal>

      </React.Fragment>
    );
  }
}
export default MainMenu;
