class serviceapi {
    get = async (API) => {

        let response = await fetch("/api/v2"+API, {
            cache: "no-cache",
            method: 'GET',
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
            },
            redirect: "follow",
            referrerPolicy: "no-referrer"
        }).then((response) => response.text())
            .then(async (responseJson) => {

                var respon = JSON.parse(responseJson);
                return respon;
            })
            .catch((error) => {
                return error;
            });

        return response;
    };

    post = async (API, body) => {

        let response = await fetch("/api/v2"+API, {
            cache: "no-cache",
            method: 'POST',
            credentials: "include",
            headers: {
                "Content-Type": "application/json",
            },
            redirect: "follow",
            referrerPolicy: "no-referrer",
            body: JSON.stringify(body)
        }).then((response) => response.text())
            .then(async (responseJson) => {

                var respon = JSON.parse(responseJson);
                return respon;
            })
            .catch((error) => {
                console.log(error);
                return error;
            });

        return response;

    };

}

export default new serviceapi;